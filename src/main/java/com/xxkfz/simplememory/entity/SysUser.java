package com.xxkfz.simplememory.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @program: xxkfz-sa-token
 * @ClassName SysUser.java
 * @author: wusongtao
 * @create: 2023-11-11 14:52
 * @description:
 * @Version 1.0
 **/
@Data
@Setter
@Getter
@ToString
public class SysUser {
    private Integer id;
    private String name;
    private int age;

}