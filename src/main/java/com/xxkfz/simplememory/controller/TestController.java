package com.xxkfz.simplememory.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: xxkfz-sa-token
 * @ClassName TestController.java
 * @author: wusongtao
 * @create: 2023-11-09 15:57
 * @description:
 * @Version 1.0
 **/
@RestController
@RequestMapping("/test/")
@Slf4j
public class TestController {

    @Autowired
    ApplicationContext applicationContext;

    @GetMapping("publish")
    public String test() {
        String msg = "消息......";
        applicationContext.publishEvent(msg);
        return "消息发送成功";
    }


    @EventListener
    public void test2(String msg) {
        log.error("接收到消息：{}", msg);

    }

}