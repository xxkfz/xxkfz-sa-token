package com.xxkfz.simplememory.controller;

/**
 * @program: xxkfz-sa-token
 * @ClassName AtCheckController.java
 * @author: wusongtao
 * @create: 2023-11-09 13:18
 * @description:
 * @Version 1.0
 **/

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.util.SaResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/check/")
public class AtCheckController {

    /*
     * 前提1：首先调用登录接口进行登录，代码在 com.pj.cases.use.LoginAuthController 中有详细解释，此处不再赘述
     * 		---- http://localhost:8081/acc/doLogin?name=zhang&pwd=123456
     *
     * 前提2：项目在配置类中注册拦截器 SaInterceptor ，代码在  com.pj.satoken.SaTokenConfigure
     * 		此拦截器将打开注解鉴权功能
     *
     * 然后我们就可以使用以下示例中的代码进行注解鉴权了
     */

    // 登录鉴权   ---- http://localhost:8081/at-check/checkLogin
    // 		登录认证后才可以进入方法
    @SaCheckLogin
    @RequestMapping("checkLogin")
    public SaResult checkLogin() {
        // 通过注解鉴权后才能进入方法 ...
        return SaResult.ok();
    }

    // 权限校验   ---- http://localhost:8081/at-check/checkPermission
    //		只有具有 user.add 权限的账号才可以进入方法
    @SaCheckPermission("user.add111")
    @RequestMapping("checkPermission")
    public SaResult checkPermission() {
        // ...
        return SaResult.ok();
    }
}