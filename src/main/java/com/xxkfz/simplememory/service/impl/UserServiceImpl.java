package com.xxkfz.simplememory.service.impl;

import com.xxkfz.simplememory.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @program: xxkfz-sa-token
 * @ClassName UserServiceImpl.java
 * @author: wusongtao
 * @create: 2023-11-07 15:07
 * @description:
 * @Version 1.0
 **/
@Service
public class UserServiceImpl implements UserService {

}